AS=nasm
LD=ld
ASFLAGS=-f elf64

all: chmod main

main: main.o dict.o lib.o
	$(LD) $^ -o $@

%.o: %.asm *.inc
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -f *.o main
	
chmod:
	chmod 755 *.asm *.inc
	
.PHONY: clean chmod
