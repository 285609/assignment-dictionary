section .text
	extern string_equals

	global find_word

	; Выполняет поиск данного ключа по словарю.
	; Если вхождение найдено, возвращает адрес начала вхождения, иначе 0
	; rdi - указатель на строку-ключ
	; rsi - указатель на последнее слово 
	; Возврат: rax - адрес начала найденого вхождения или 0
	find_word:
		push r12
		push r13
		mov r12, rsi ; r12 - указатель на текущее слово в словаре
		mov r13, rdi ; r13 - указатель на строку-ключ
		.loop:
			test r12, r12
			jz .failure
			lea rdi, [r12 + 8]
			mov rsi, r13
			call string_equals
			test rax, rax
			jnz .success
			mov r12, [r12]
			jmp .loop
		.success:
			mov rax, r12
			jmp .exit
		.failure:
			xor rax, rax
		.exit:
			pop r13
			pop r12
			ret
		
