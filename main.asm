section .text
	%include "colon.inc"
	%include "lib.inc"
	
	%define BUFFER_SIZE 256
	
	%define STDOUT 1
	%define STDERR 2

	extern find_word

	global _start
	
section .rodata
	%include "words.inc"
	not_found_error: db "Ошибка! Слово не найдено.", 0
	input_word_msg: db "Введите ключ: ", 0

section .text
	_start:
		push r12
		push r13
		sub rsp, BUFFER_SIZE
		mov r12, rsp ; r12 - указатель на выделенный буфер 
		.loop:
			mov rdi, input_word_msg
			mov rsi, STDOUT
			call print_string
			mov rdi, rsp
			mov rsi, BUFFER_SIZE
			call read_word
			mov rdi, rax
			call string_length
			test rax, rax
			jz .exit
			mov rdi, r12 
			mov rsi, last_word
			call find_word
			test rax, rax
			jz .failure
			lea r13, [rax + 8] ; r13 - указатель на найденное вхождение
			mov rdi, r13
			call string_length
			lea rdi, [r13 + rax + 1]
			mov rsi, STDOUT
			call print_string
			call print_newline
			jmp .loop
		.failure:
			mov rdi, not_found_error
			mov rsi, STDERR
			call print_string
			call print_newline
			jmp .loop
		.exit:
			call print_newline
			add rsp, BUFFER_SIZE
			pop r13
			pop r12
			mov rdi, 0
			jmp exit
			
