section .text
%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define STDIN 0
%define STDOUT 1
%define STDERR 2

%define CHAR_NEW_LINE 0xA
%define CHAR_TAB 0x9

%define UINT64_STR_LEN 20

global exit
global string_length
global print_char
global print_newline
global print_string
global print_uint
global print_int
global string_equals
global parse_uint
global parse_int
global read_word
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, SYS_EXIT
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi - указатель на начало строки
string_length:
	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		ret

; Принимает указатель на нуль-терминированную строку, выводит её в поток с дескриптором $rsi
; rdi - указатель на начало строки
; rsi - дескриптор потока вывода
print_string:
	push rdi
	push rsi
	call string_length
	pop rdi
	pop rsi
	mov rdx, rax
	mov rax, SYS_WRITE
	syscall
	ret
	
; Принимает код символа и выводит его в stdout
; rdi - код символа
print_char:
	push rdi
	mov rsi, rsp
	mov rax, SYS_WRITE
	mov rdi, STDOUT
	mov rdx, 1
	syscall
	pop rdi
	ret
	
; Переводит строку
print_newline:
	mov rdi, CHAR_NEW_LINE
	jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; rdi - число
print_uint:
	xor r9, r9
	push 0
	mov r10, rsp ; указатель на текущий символ
	sub rsp, UINT64_STR_LEN
	mov r8, 10 ; делитель
	.loop:
		dec r10
		xor rdx, rdx
		mov rax, rdi
		div r8
		mov rdi, rax
		
		mov r9b, '0'
		add r9, rdx
		mov byte[r10], r9b
		
		test rdi, rdi
		jnz .loop
	.end:
		mov rdi, r10
		mov rsi, STDOUT
		call print_string
		add rsp, UINT64_STR_LEN + 1
		ret
	
; Выводит знаковое 8-байтовое число в десятичном формате 
; rdi - число
print_int:
    test rdi, rdi
	jge print_uint
	neg rdi
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	jmp print_uint
	
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rdi, rsi - два указателя на строки
string_equals:
	mov r8b, byte [rdi]
	cmp r8b, byte [rsi]
	jne .not_equals
	test r8b, r8b
	jz .equals
	inc rdi
	inc rsi
	jmp string_equals
	.not_equals:
		xor rax, rax
		ret
	.equals:
		mov rax, 1
		ret
	

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push byte 0
	mov rax, SYS_READ
	mov rdi, STDIN
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret 
	
; проверяет символ в rdi пробельный он или нет
is_space_char:
	mov rdx, rdi
	cmp rdi, ' '
	jz .true
	cmp rdi, CHAR_TAB
	jz .true
	cmp rdi, CHAR_NEW_LINE
	jz .true
	.false:
		mov rax, 0
		ret
	.true:
		mov rax, 1
		ret 
		
; читает пробелы до первого непробельного символа, если есть, его и возвращает	
read_spaces:
	push r12
	.loop:
		call read_char
		mov r12, rax
		mov rdi, rax
		call is_space_char
		test rax, rax
		jnz .loop
	mov rax, r12
	pop r12
	ret
	
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
; rdi - адрес начала буфера, rsi - размер буфера
read_word:
	push r12
	push r13
	push r14
	; r12 - адрес буфера, r13 - размер
	; r14 - кол-во прочитанных символов
	mov r12, rdi
	mov r13, rsi - 1
	test r13, r13
	jle .failure
	mov r14, 0
	
	; чтение и пропуск начальных пробелов
	call read_spaces
	test rax, rax
	jz .success
	
	.loop:
		; проверка, кончился ли буфер
		test r13, r13
		jz .failure
		mov byte [r12+r14], al
		inc r14
		dec r13
		call read_char
		test rax, rax
		jz .success
		mov rdi, rax
		call is_space_char
		test rax, rax
		; если символ пробельный (rax = 1), то окончание ввода
		jnz .success
		mov rax, rdx
		jmp .loop

	.failure:
		mov rax, 0
		jmp .exit
		
	.success:
		mov byte [r12+r14], 0
		mov rax, r12
		mov rdx, r14
	.exit:
		pop r14 
		pop r13
		pop r12
		ret
		
; проверка, является ли символ десятичной цифрой		
is_digit:
	xor r8, r8
	xor r9, r9
	mov r8b, '0'
	mov r9b, '9'
	cmp dil, r8b
	jl .not_digit
	cmp dil, r9b
	jg .not_digit
	mov rdx, rdi
	sub rdx, r8
	.digit:
		mov rax, 1
		ret
	.not_digit:
		mov rax, 0
		ret
		
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	push r12
	push r13
	push r14
	push r15
	xor r13, r13
	mov r12, rdi ; указатель на очередной символ
	mov r13b, byte [r12] ; текущий символ
	xor r14, r14 ; счетчик
	xor r15, r15 ; число
	.loop:
		test r13, r13
		jz .end ; заканчиваем парсинг, если нуль-терминатор
		mov rdi, r13
		call is_digit
		test rax, rax
		jz .end
		imul r15, r15, 10
		add r15, rdx
		inc r14
		mov r13b, byte [r12+r14]
		jmp .loop
	.end:
		mov rdx, r14
		mov rax, r15
		pop r15
		pop r14
		pop r13
		pop r12
		ret
		
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	push r13
	push r14
	xor r13, r13 ; указывает, отрицательное ли число
	xor r14, r14 ; указывает, есть ли знак
	cmp byte [rdi], '-' ; rdi - указатель на текущий символ
	je .negative
	cmp byte [rdi], '+'
	je .with_sign
	jmp .parse_uint
	
	.negative:
		inc r13
		
	.with_sign:
		inc r14
		inc rdi
		
	.parse_uint:
		call parse_uint
		test rdx, rdx
		jz .exit
		add rdx, r14
		test r13, r13
		jz .exit
		neg rax
	
	.exit: 
		pop r14
		pop r13
		ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rax, rax
	xor r8, r8
	
	.loop:
		test rdx, rdx
		jz .end_buffer
		mov r8, [rdi+rax]
		mov [rsi+rax], r8
		test sil, sil
		jz .exit
		inc rax
		dec rdx
		jmp .loop

	.end_buffer:
		xor rax, rax
	
	.exit:
		ret
	