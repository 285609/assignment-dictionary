%define last_word 0
%macro colon 2
	%ifstr %1
		%%new_word: dq last_word
		db %1, 0
		%2:
		%define last_word %%new_word
	%else 
		%error "First colon arg must be string"
	%endif
%endmacro
